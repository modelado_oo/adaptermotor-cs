﻿using AdapterDAO.AdaptadoresMotores;
using AdapterDAO.Motores;
using AdapterDAO.Vehiculos;
using System;

namespace Adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Motor Economico
            Automovil automovil = new Automovil("Fiesta", "Ford", new MotorEconomico());
            automovil.VelocidadAceleracion = Aceleracion.Media;

            Console.WriteLine(automovil);
            automovil.Recorrido();
            #endregion

            #region Motor Común
            automovil = new Automovil("Sentra", "Nissan", new MotorComun());
            automovil.VelocidadAceleracion = Aceleracion.Alta;

            Console.WriteLine(automovil);
            automovil.Recorrido();
            #endregion

            #region Motor Eléctrico
            IMotorElectrico motorElectrico = new MotorElectricoExperimental();
            MotorElectricoAdapter adaptador = new MotorElectricoAdapter(motorElectrico);
            automovil = new Automovil("Tesla 1", "Tesla", adaptador);

            Console.WriteLine(automovil);
            automovil.Recorrido();
            #endregion

            Console.ReadKey();
        }
    }
}

﻿using System;
using AdapterDAO.AdaptadoresMotores;
using AdapterDAO.Motores;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdapterTest
{
    [TestClass]
    public class MotorElectricoAdapterTest
    {
        MotorElectricoAdapter adaptador;
        MotorElectricoExperimental motorElectrico;

        // Equivalente al @Before
        [TestInitialize]
        public void TestInitialize()
        {
            motorElectrico = new MotorElectricoExperimental();
            motorElectrico.Activar();

            MotorElectricoExperimental motorElectricoParaAdaptador = new MotorElectricoExperimental();
            motorElectricoParaAdaptador.Activar();

            adaptador = new MotorElectricoAdapter(motorElectricoParaAdaptador);
        }

        [TestMethod]
        public void MotorElectrico_ComportamientoIgual_MotorElectricoAdapter()
        {
            Aceleracion aceleracion = Aceleracion.Baja;

            Assert.AreEqual(motorElectrico.MoverMasRapido(aceleracion), adaptador.Acelerar(aceleracion));
        }
    }
}

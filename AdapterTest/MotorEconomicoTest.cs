﻿using System;
using AdapterDAO.Motores;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdapterTest
{
    [TestClass]
    public class MotorEconomicoTest
    {
        IMotor MotorEconomico;

        [TestInitialize]
        public void TestInitialize()
        {
            MotorEconomico = new MotorEconomico();
            MotorEconomico.Encender();
        }

        // Equivalente al @After
        [TestCleanup]
        public void TestCleanUp()
        {
            MotorEconomico.Apagar();
        }

        [TestMethod]
        public void MotorEconomico_Aceleracion_Baja()
        {
            Assert.AreEqual(10.0d, MotorEconomico.Acelerar(Aceleracion.Baja));
        }

        [TestMethod]
        public void MotorEconomico_Aceleracion_Media()
        {
            Assert.AreEqual(30.0d, MotorEconomico.Acelerar(Aceleracion.Media));
        }

        [TestMethod]
        public void MotorEconomico_Aceleracion_Alta()
        {
            Assert.AreEqual(50.0d, MotorEconomico.Acelerar(Aceleracion.Alta));
        }
    }
}

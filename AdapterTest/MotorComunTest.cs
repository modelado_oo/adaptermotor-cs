﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AdapterDAO.Motores;

namespace AdapterTest
{
    [TestClass]
    public class MotorComunTest
    {
        IMotor MotorComun;

        // Equivalente al @Before
        [TestInitialize]
        public void TestInitialize()
        {
            MotorComun = new MotorComun();
            MotorComun.Encender();
        }

        // Equivalente al @After
        [TestCleanup]
        public void TestCleanUp()
        {
            MotorComun.Apagar();
        }

        [TestMethod]
        public void MotorComun_Aceleracion_Baja()
        {
            Assert.AreEqual(20.0d, MotorComun.Acelerar(Aceleracion.Baja));
        }

        [TestMethod]
        public void MotorComun_Aceleracion_Media()
        {
            Assert.AreEqual(50.0d, MotorComun.Acelerar(Aceleracion.Media));
        }

        [TestMethod]
        public void MotorComun_Aceleracion_Alta()
        {
            Assert.AreEqual(80.0d, MotorComun.Acelerar(Aceleracion.Alta));
        }
    }
}

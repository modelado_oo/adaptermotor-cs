﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdapterDAO.Motores
{
    public class MotorComun : IMotor
    {
        public double Acelerar(Aceleracion aceleracion)
        {
            double velocidad = 0;

            switch (aceleracion)
            {
                case Aceleracion.Baja:
                    velocidad = 20;
                    break;
                case Aceleracion.Media:
                    velocidad = 50;
                    break;
                case Aceleracion.Alta:
                    velocidad = 80;
                    break;
            }

            Console.WriteLine("Motor común acelerando a " + velocidad + " km/h...");
            return velocidad;
        }

        public void Apagar()
        {
            Console.WriteLine("Motor común apagado...");
        }

        public void Encender()
        {
            Console.WriteLine("Motor común encendido...");
        }
    }
}

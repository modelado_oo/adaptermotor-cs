﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterDAO.Motores
{
    public interface IMotorElectrico
    {
        void Activar();
        void Conectar();
        void Desconectar();
        void Detener();
        double MoverMasRapido(Aceleracion aceleracion);
    }
}

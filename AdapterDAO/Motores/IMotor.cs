﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdapterDAO.Motores
{
    public enum Aceleracion
    {
        Baja,
        Media,
        Alta
    }

    public interface IMotor
    {
        void Encender();
        void Apagar();
        double Acelerar(Aceleracion aceleracion);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterDAO.Motores
{
    public static class RecorridoExtension
    {
        #region Extension Methods IMotor
        public static void Recorrido(this IMotor motor, Aceleracion aceleracion)
        {
            Console.WriteLine("Realizando secuencia de recorrido...");
        
            motor.Encender();
            motor.Acelerar(aceleracion);
            motor.Apagar();

            Console.WriteLine("Recorrido finalizado...\n");
        }
        #endregion
    }
}

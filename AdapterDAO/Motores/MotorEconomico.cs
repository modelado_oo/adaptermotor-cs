﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterDAO.Motores
{
    public class MotorEconomico : IMotor
    {
        public double Acelerar(Aceleracion aceleracion)
        {
            double velocidad = 0;

            switch (aceleracion)
            {
                case Aceleracion.Baja:
                    velocidad = 10;
                    break;
                case Aceleracion.Media:
                    velocidad = 30;
                    break;
                case Aceleracion.Alta:
                    velocidad = 50;
                    break;
            }

            Console.WriteLine("Motor económico acelerando a " + velocidad + " km/h...");
            return velocidad;
        }

        public void Apagar()
        {
            Console.WriteLine("Motor económico apagado...");
        }

        public void Encender()
        {
            Console.WriteLine("Motor económico encendido...");
        }
    }
}

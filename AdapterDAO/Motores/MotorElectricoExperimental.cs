﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterDAO.Motores
{
    public class MotorElectricoExperimental : IMotorElectrico
    {
        public void Activar()
        {
            Console.WriteLine("Motor eléctrico activado...");
        }

        public void Conectar()
        {
            Console.WriteLine("Motor eléctrico conectado...");
        }

        public void Desconectar()
        {
            Console.WriteLine("Motor eléctrico desconectado...");
        }

        public void Detener()
        {
            Console.WriteLine("Motor eléctrico detenido...");
        }

        public double MoverMasRapido(Aceleracion aceleracion)
        {
            double velocidad = 0;

            switch (aceleracion)
            {
                case Aceleracion.Baja:
                    velocidad = 25;
                    break;
                case Aceleracion.Media:
                    velocidad = 45;
                    break;
                case Aceleracion.Alta:
                    velocidad = 65;
                    break;
            }

            Console.WriteLine("Motor eléctrico acelerando a " + velocidad + " km/h...");
            return velocidad;
        }
    }
}

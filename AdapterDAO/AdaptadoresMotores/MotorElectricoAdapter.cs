﻿using AdapterDAO.Motores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterDAO.AdaptadoresMotores
{
    public class MotorElectricoAdapter : IMotor
    {
        IMotorElectrico MotorElectrico;

        public MotorElectricoAdapter(IMotorElectrico motorElectrico)
        {
            MotorElectrico = motorElectrico;
        }

        public double Acelerar(Aceleracion aceleracion)
        {
            return MotorElectrico.MoverMasRapido(aceleracion);
        }

        public void Apagar()
        {
            MotorElectrico.Detener();
        }

        public void Encender()
        {
            MotorElectrico.Activar();
        }
    }
}

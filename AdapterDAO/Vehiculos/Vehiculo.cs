﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterDAO.Vehiculos
{
    public abstract class Vehiculo
    {
        public string Marca { get; set; }
        public string Nombre { get; set; }
    }
}

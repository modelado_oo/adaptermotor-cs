﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterDAO.Vehiculos
{
    public interface IMotorizable
    {
        void Acelerar();
        void Apagar();
        void Encender();
    }
}
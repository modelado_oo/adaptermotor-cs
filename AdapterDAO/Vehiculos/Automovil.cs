﻿using AdapterDAO.Motores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterDAO.Vehiculos
{
    public class Automovil : VehiculoMotorizado
    {
        public Aceleracion VelocidadAceleracion { get; set; }

        public Automovil(String nombre, String marca, IMotor motor)
        {
            Nombre = nombre;
            Marca = marca;
            Motor = motor;
            VelocidadAceleracion = Aceleracion.Baja;
        }

        public override void Acelerar()
        {
            Motor.Acelerar(VelocidadAceleracion);
        }

        public override void Apagar()
        {
            Motor.Apagar();
        }

        public override void Encender()
        {
            Motor.Encender();
        }

        public override string ToString()
        {
            return "Automóvil " + Nombre + ", " + Marca + " con motor " + Motor + " a velocidad " + VelocidadAceleracion + ".";
        }

        public override void Recorrido()
        {
            Motor.Recorrido(VelocidadAceleracion);
        }
    }
}

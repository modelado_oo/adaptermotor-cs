﻿using AdapterDAO.Motores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterDAO.Vehiculos
{
    public abstract class VehiculoMotorizado : Vehiculo, IMotorizable
    {
        public IMotor Motor { get; set; }
        
        public abstract void Acelerar();
        public abstract void Apagar();
        public abstract void Encender();
        public abstract void Recorrido();
    }
}
